package com.erayasan.socio_sample.model;

/**
 * Created by erayasan on 13/03/2018.
 */

public class FilterProps {

    private int lowerAgeLimit, higherAgeLimit;

    public FilterProps(int lowerAgeLimit, int higherAgeLimit) {
        this.lowerAgeLimit = lowerAgeLimit;
        this.higherAgeLimit = higherAgeLimit;
    }

    public int getLowerAgeLimit() {
        return lowerAgeLimit;
    }

    public void setLowerAgeLimit(int lowerAgeLimit) {
        this.lowerAgeLimit = lowerAgeLimit;
    }

    public int getHigherAgeLimit() {
        return higherAgeLimit;
    }

    public void setHigherAgeLimit(int higherAgeLimit) {
        this.higherAgeLimit = higherAgeLimit;
    }
}
