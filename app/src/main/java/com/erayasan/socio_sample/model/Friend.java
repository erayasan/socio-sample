package com.erayasan.socio_sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by erayasan on 27/02/2018.
 */

public class Friend extends RealmObject implements Parcelable {
    @PrimaryKey
    @SerializedName("id")
    protected String id;
    @SerializedName("name")
    protected String name;

    public Friend() {
        // must exist to use realm and parcelable together
    }

    protected Friend(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    //Parcelable implementation
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Friend> CREATOR = new Creator<Friend>() {
        @Override
        public Friend createFromParcel(Parcel in) {
            return new Friend(in);
        }

        @Override
        public Friend[] newArray(int size) {
            return new Friend[size];
        }
    };


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
    }
}
