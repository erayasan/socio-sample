package com.erayasan.socio_sample.network.service;

import com.erayasan.socio_sample.model.UserProfile;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by erayasan on 12/02/2018.
 */

public interface UserProfileService {

    @GET("41Na_lBI4")
    Single<List<UserProfile>> getUserProfiles();

}
