package com.erayasan.socio_sample.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.erayasan.socio_sample.core.SocioApplication;
import com.erayasan.socio_sample.db.dao.UserProfileDao;
import com.erayasan.socio_sample.db.helper.RealmLiveData;
import com.erayasan.socio_sample.model.FilterProps;
import com.erayasan.socio_sample.model.UserProfile;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by erayasan on 19/02/2018.
 */

public class MainViewModel extends ViewModel {

    private RealmLiveData<UserProfile> liveUserProfileRealmResults;
    private CompositeDisposable compositeDisposable;
    private UserProfileDao userProfileDao;

    private FilterProps filterProps;
    private String lastSearchedText;

    public MainViewModel() {
        compositeDisposable = new CompositeDisposable();
        userProfileDao = new UserProfileDao(Realm.getDefaultInstance());
        liveUserProfileRealmResults = userProfileDao.loadAllAsLiveData();
    }

    @Override
    protected void onCleared() {
        super.onCleared();

        if (userProfileDao != null)
            userProfileDao.close();

        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public void requestUserProfilesFromNetwork() {

        liveUserProfileRealmResults.setValue(userProfileDao.loadAll());

        compositeDisposable.add(
                SocioApplication
                        .getInstance()
                        .getNetworkManager()
                        .getUserProfileService()
                        .getUserProfiles()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::saveResponse, this::handleError));
    }

    public RealmLiveData<UserProfile> getUserProfiles() {
        return liveUserProfileRealmResults;
    }

    private void saveResponse(List<UserProfile> userProfiles) {
        userProfileDao.saveAsync(userProfiles, null);
    }

    private void handleError(Throwable throwable) {
        throwable.printStackTrace();
    }

    public void instantSearch(String text) {

        lastSearchedText = text;

        Flowable.just(text)
                .debounce(3, TimeUnit.SECONDS)
                .distinctUntilChanged()
                .switchMap(s -> userProfileDao.filterUsers(filterProps, text).asFlowable())
                .filter(RealmResults::isLoaded)
                .doOnError(Throwable::printStackTrace)
                .subscribe(userProfiles -> liveUserProfileRealmResults.setValue(userProfiles), this::handleError);

    }

    public void actionFilterClicked(FilterProps filterProps) {
        this.filterProps = filterProps;
        liveUserProfileRealmResults.setValue(userProfileDao.filterUsers(filterProps, lastSearchedText));
    }

    public FilterProps getFilterProps() {
        return filterProps;
    }

}
