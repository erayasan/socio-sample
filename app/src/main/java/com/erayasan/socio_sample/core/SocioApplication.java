package com.erayasan.socio_sample.core;

import android.app.Application;

import com.erayasan.socio_sample.network.NetworkManager;

import io.realm.Realm;

/**
 * Created by erayasan on 08/02/2018.
 */

public class SocioApplication extends Application {

    private static SocioApplication instance;

    private NetworkManager networkManager;

    public static SocioApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        Realm.init(this);
    }

    public NetworkManager getNetworkManager() {
        if (networkManager == null)
            networkManager = new NetworkManager();
        return networkManager;
    }
}
