package com.erayasan.socio_sample.core;

import android.support.annotation.Nullable;

import com.erayasan.socio_sample.db.helper.RealmAsyncCallback;
import com.erayasan.socio_sample.db.helper.RealmLiveData;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by erayasan on 27/02/2018.
 */

public class BaseRealmDao<T extends RealmObject> {

    protected Realm realm;
    private Class<T> realmObjectClass;

    public BaseRealmDao(Realm realm, Class<T> realmObjectClass) {
        this.realm = realm;
        this.realmObjectClass = realmObjectClass;
    }

    public void save(final T realmObject) {
        try {
            realm.executeTransaction((realm) -> realm.insertOrUpdate(realmObject));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void save(final List<T> realmObjects) {
        try {
            realm.executeTransaction((realm) -> realm.insertOrUpdate(realmObjects));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public RealmAsyncTask saveAsync(final T realmObject, @Nullable RealmAsyncCallback callback) {
        try (Realm realmInstance = Realm.getInstance(realm.getConfiguration())) {
            return realmInstance.executeTransactionAsync(
                    (realm) -> realm.insertOrUpdate(realmObject),
                    callback != null ? callback::onSuccess : null,
                    callback != null ? callback::onError : null);
        }
    }

    public RealmAsyncTask saveAsync(final List<T> realmObjects, @Nullable RealmAsyncCallback callback) {
        try (Realm realmInstance = Realm.getInstance(realm.getConfiguration())) {
            return realmInstance.executeTransactionAsync(
                    (realm) -> realm.insertOrUpdate(realmObjects),
                    callback != null ? callback::onSuccess : null,
                    callback != null ? callback::onError : null);
        }
    }

    public RealmLiveData<T> loadAllAsLiveData() {
        return new RealmLiveData<>(loadAll());
    }

    public RealmLiveData<T> loadAllAsLiveDataSortedAsync(String sortKey) {
        return new RealmLiveData<>(loadAllSortedAsync(sortKey));
    }

    public RealmLiveData<T> loadAllAsLiveDataSorted(String sortKey) {
        return new RealmLiveData<>(loadAllSorted(sortKey));
    }

    public RealmLiveData<T> loadAllAsLiveDataAsync() {
        return new RealmLiveData<>(loadAllAsync());
    }

    public RealmResults<T> loadAll() {
        return realm.where(realmObjectClass).findAll();
    }

    private RealmResults<T> loadAllAsync() {
        return realm.where(realmObjectClass).findAllAsync();
    }

    private RealmResults<T> loadAllSorted(String sortKey) {
        return realm.where(realmObjectClass).sort(sortKey).findAll();
    }

    private RealmResults<T> loadAllSortedAsync(String sortKey) {
        return realm.where(realmObjectClass).sort(sortKey).findAllAsync();
    }

    public void removeAll() {
        try (Realm realmInstance = realm) {
            realmInstance.executeTransaction((realm) -> realm.delete(realmObjectClass));
        }
    }

    public long count() {
        return realm.where(realmObjectClass).count();
    }

    public void close() {
        realm.close();
    }

}
