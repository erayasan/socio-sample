package com.erayasan.socio_sample.core;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by erayasan on 19/02/2018.
 */

public abstract class BaseRecyclerAdapter<T, VH extends BaseRecyclerAdapter.BaseViewHolder> extends RecyclerView.Adapter<VH> {

    public List<T> dataSet;

    public BaseRecyclerAdapter() {

    }

    public BaseRecyclerAdapter(List<T> dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(VH holder, int position) {
        holder.onBind(dataSet.get(position));
    }


    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public abstract void setList(List<T> list);

    public abstract static class BaseViewHolder<T> extends RecyclerView.ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public abstract void onBind(T model);
    }

}