package com.erayasan.socio_sample.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.erayasan.socio_sample.R;

import java.lang.reflect.Field;

import io.codetail.animation.ViewAnimationUtils;

/**
 * Created by erayasan on 03/03/2018.
 */

public class SearchToolbar extends FrameLayout {

    public interface OnSearchToolbarTextChangedListener {
        void onQueryTextChange(String text);

        void onQueryTextSubmit(String query);
    }

    private Toolbar toolbar, searchToolbar;
    private Menu search_menu;
    private MenuItem item_search;
    private OnSearchToolbarTextChangedListener searchToolbarTextChangedListener;


    public SearchToolbar(@NonNull Context context) {
        this(context, null);
    }

    public SearchToolbar(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchToolbar(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        toolbar = LayoutInflater.from(context).inflate(R.layout.toolbar, this, true).findViewById(R.id.lib_toolbar);
        searchToolbar = LayoutInflater.from(context).inflate(R.layout.search_toolbar, this, true).findViewById(R.id.lib_search_toolbar);

        initToolbar();
        initSearchToolbar();

    }

    private void initToolbar() {

        toolbar.inflateMenu(R.menu.menu_search_main);

        toolbar.setOnMenuItemClickListener(item -> {
            circleReveal(R.id.lib_search_toolbar, 0, true, true);

            item_search.expandActionView();
            return true;
        });
    }

    private void initSearchToolbar() {
        if (searchToolbar != null) {

            searchToolbar.inflateMenu(R.menu.menu_search_inner);
            search_menu = searchToolbar.getMenu();

            searchToolbar.setNavigationOnClickListener(v -> circleReveal(R.id.lib_search_toolbar, 0, true, false));

            item_search = search_menu.findItem(R.id.action_filter_search);

            item_search.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem menuItem) {
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                    circleReveal(R.id.lib_search_toolbar, 0, true, false);
                    return true;
                }
            });

            initSearchView();

        } else
            Log.e(SearchToolbar.class.getSimpleName(), "SearchToolbar should not be null!");
    }


    public void initSearchView() {

        final SearchView searchView = (SearchView) search_menu.findItem(R.id.action_filter_search).getActionView();

        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setSubmitButtonEnabled(false);

        ImageView closeButton = searchView.findViewById(R.id.search_close_btn);
        closeButton.setImageResource(R.drawable.ic_close);
        closeButton.setPadding(0, 0, 0, 0);

        EditText txtSearch = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        txtSearch.setHint(TextUtils.concat(getResources().getString(android.R.string.search_go), "..."));
        txtSearch.setHintTextColor(Color.DKGRAY);
        txtSearch.setTextColor(getResources().getColor(R.color.colorPrimary));

        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(txtSearch, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (searchToolbarTextChangedListener != null) {
                    searchToolbarTextChangedListener.onQueryTextSubmit(query);
                }
                searchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (searchToolbarTextChangedListener != null) {
                    searchToolbarTextChangedListener.onQueryTextChange(newText);
                }
                return true;
            }

        });

    }

    public void circleReveal(int viewID, int posFromRight, boolean containsOverflow, final boolean isShow) {
        final View myView = findViewById(viewID);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            this.setLayerType(LAYER_TYPE_SOFTWARE, null);
            getRootView().setLayerType(LAYER_TYPE_SOFTWARE, null);
            myView.setLayerType(LAYER_TYPE_SOFTWARE, null);
        }

        int width = myView.getWidth();

        if (posFromRight > 0)
            width -= (posFromRight * getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)) - (getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) / 2);
        if (containsOverflow)
            width -= getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);

        int cx = width;
        int cy = myView.getHeight() / 2;

        Animator animator;
        if (isShow)
            animator = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, (float) width);
        else
            animator = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float) width, 0);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());

        animator.setDuration(300);

        // make the view invisible when the animation is done
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (!isShow) {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.INVISIBLE);
                }
            }
        });

        // make the view visible and start the animation
        if (isShow)
            myView.setVisibility(View.VISIBLE);

        // start the animation
        animator.start();

    }

    public void setOnSearchToolbarTextChangedListener(OnSearchToolbarTextChangedListener searchToolbarTextChangedListener) {
        this.searchToolbarTextChangedListener = searchToolbarTextChangedListener;
    }

}
