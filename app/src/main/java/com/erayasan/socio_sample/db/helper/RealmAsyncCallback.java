package com.erayasan.socio_sample.db.helper;

/**
 * Created by erayasan on 01/03/2018.
 */

public interface RealmAsyncCallback {

    void onSuccess();

    void onError(Throwable t);
}
