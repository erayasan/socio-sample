package com.erayasan.socio_sample.db.dao;

import android.text.TextUtils;

import com.erayasan.socio_sample.core.BaseRealmDao;
import com.erayasan.socio_sample.model.FilterProps;
import com.erayasan.socio_sample.model.UserProfile;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by erayasan on 27/02/2018.
 */

public class UserProfileDao extends BaseRealmDao<UserProfile> {

    public UserProfileDao(Realm realm) {
        super(realm, UserProfile.class);
    }

    public RealmResults<UserProfile> filterFullName(String value) {

        String[] namePairs = value.split(" ");

        RealmQuery<UserProfile> query = realm
                .where(UserProfile.class)
                .beginsWith("firstName", namePairs.length > 0 ? namePairs[0] : value, Case.INSENSITIVE);

        if (namePairs.length > 1) {
            query = query
                    .and()
                    .beginsWith("lastName", namePairs[1], Case.INSENSITIVE);
        }

        return query.findAll();
    }

    public RealmResults<UserProfile> filterUsers(FilterProps filterProps, String lastSearchedText) {

        if (!TextUtils.isEmpty(lastSearchedText)) {
            if (filterProps != null) {
                return filterFullName(lastSearchedText)
                        .where()
                        .between("age", filterProps.getLowerAgeLimit(), filterProps.getHigherAgeLimit())
                        .findAll();
            } else {
                return filterFullName(lastSearchedText);
            }
        } else {
            return realm
                    .where(UserProfile.class)
                    .between("age", filterProps.getLowerAgeLimit(), filterProps.getHigherAgeLimit())
                    .findAll();
        }
    }
}
