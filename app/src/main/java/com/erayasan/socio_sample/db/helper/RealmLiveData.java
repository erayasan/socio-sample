package com.erayasan.socio_sample.db.helper;

import android.arch.lifecycle.MutableLiveData;

import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import io.realm.RealmResults;

/**
 * Created by erayasan on 01/03/2018.
 */

public class RealmLiveData<T extends RealmModel> extends MutableLiveData<RealmResults<T>> {

    private RealmResults<T> results;

    private final RealmChangeListener<RealmResults<T>> listener;

    public RealmLiveData(RealmResults<T> realmResults) {
        results = realmResults;
        listener = this::setValue;
    }

    @Override
    protected void onActive() {
        results.addChangeListener(listener);
    }

    @Override
    protected void onInactive() {
        results.removeChangeListener(listener);
    }

}