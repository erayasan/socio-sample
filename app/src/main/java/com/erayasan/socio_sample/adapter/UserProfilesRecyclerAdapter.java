package com.erayasan.socio_sample.adapter;

import android.support.v7.util.DiffUtil;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.erayasan.socio_sample.R;
import com.erayasan.socio_sample.core.BaseRecyclerAdapter;
import com.erayasan.socio_sample.fragment.UserProfileDetailDialogFragment;
import com.erayasan.socio_sample.model.UserProfile;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by erayasan on 19/02/2018.
 */

public class UserProfilesRecyclerAdapter extends BaseRecyclerAdapter<UserProfile, UserProfilesRecyclerAdapter.UserProfileViewHolder> {

    public UserProfilesRecyclerAdapter() {
        super();
    }

    @Override
    public UserProfilesRecyclerAdapter.UserProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserProfileViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_profile, parent, false));
    }

    @Override
    public void setList(final List<UserProfile> userProfiles) {
        if (dataSet == null) {
            dataSet = userProfiles;
            notifyItemRangeInserted(0, userProfiles.size());
        } else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return dataSet.size();
                }

                @Override
                public int getNewListSize() {
                    return userProfiles.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    UserProfile oldProfile = dataSet.get(oldItemPosition);
                    UserProfile newProfile = userProfiles.get(newItemPosition);

                    return TextUtils.equals(oldProfile.getId(), newProfile.getId());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    UserProfile oldProfile = dataSet.get(oldItemPosition);
                    UserProfile newProfile = userProfiles.get(newItemPosition);

                    return TextUtils.equals(oldProfile.getId(), newProfile.getId())
                            && TextUtils.equals(oldProfile.getFirstName(), newProfile.getFirstName())
                            && TextUtils.equals(oldProfile.getLastName(), newProfile.getLastName())
                            && TextUtils.equals(oldProfile.getPicture(), newProfile.getPicture())
                            && TextUtils.equals(oldProfile.getCompany(), newProfile.getCompany());
                }
            });
            dataSet = userProfiles;
            diffResult.dispatchUpdatesTo(this);
        }
    }

    static class UserProfileViewHolder extends BaseRecyclerAdapter.BaseViewHolder<UserProfile> {

        @BindView(R.id.txt_name)
        TextView fullName;
        @BindView(R.id.txt_company)
        TextView company;
        @BindView(R.id.img_profile_pic)
        ImageView profilePic;

        private UserProfile currentUserProfile;

        UserProfileViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void onBind(UserProfile model) {
            currentUserProfile = model;

            fullName.setText(model.getFullName());
            company.setText(model.getCompany());
            Glide.with(itemView.getContext()).load(model.getPicture()).into(profilePic);
        }

        @OnClick(R.id.card_user_profile)
        void onUserProfileCardClicked() {
            UserProfileDetailDialogFragment.newInstance(currentUserProfile).show(itemView.getContext());
        }
    }
}
