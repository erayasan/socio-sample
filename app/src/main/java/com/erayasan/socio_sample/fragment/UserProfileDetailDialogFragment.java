package com.erayasan.socio_sample.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.erayasan.socio_sample.R;
import com.erayasan.socio_sample.model.UserProfile;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class UserProfileDetailDialogFragment extends AppCompatDialogFragment {

    private static final String BUNDLE_KEY_USER_PROFILE = "detail_dialog_user_profile";
    private static final String TAG = UserProfileDetailDialogFragment.class.getSimpleName();

    @BindView(R.id.img_user_profile)
    ImageView userProfileImageView;
    @BindView(R.id.txt_user_name)
    TextView userNameTextView;
    @BindView(R.id.txt_user_title)
    TextView userTitleTextView;
    @BindView(R.id.txt_user_company)
    TextView userCompanyTextView;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    private Unbinder unbinder;

    private UserProfile userProfile;

    public static UserProfileDetailDialogFragment newInstance(UserProfile userProfile) {
        UserProfileDetailDialogFragment fragment = new UserProfileDetailDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUNDLE_KEY_USER_PROFILE, userProfile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userProfile = getArguments().getParcelable(BUNDLE_KEY_USER_PROFILE);
        }
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        Point size = new Point();

        if (window != null) {
            Display display = window.getWindowManager().getDefaultDisplay();
            display.getSize(size);
            // Set the width of the dialog proportional to 75% of the screen width

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                window.setLayout((int) (size.x * 0.95), WindowManager.LayoutParams.WRAP_CONTENT);
            else
                window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

            window.setGravity(Gravity.CENTER);
            // Call super onResume after sizing
        }
        super.onResume();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile_detail_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (userProfile != null) {
            userNameTextView.setText(TextUtils.concat(userProfile.getFullName(), ", ", userProfile.getAge().toString()));
            userTitleTextView.setText(userProfile.getAbout());
            userCompanyTextView.setText(userProfile.getCompany());
            Glide.with(UserProfileDetailDialogFragment.this).load(userProfile.getPicture()).into(userProfileImageView);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_user_add)
    public void onViewClicked() {
        Toast.makeText(getContext(), userProfile.getFullName() + " " + getString(R.string.friend_added_success), Toast.LENGTH_LONG).show();
        dismiss();
    }

    public void show(@NonNull Context context) {
        if (context instanceof AppCompatActivity)
            this.show(((AppCompatActivity) context).getSupportFragmentManager(), TAG);
        else if (context instanceof FragmentActivity)
            this.show(((FragmentActivity) context).getSupportFragmentManager(), TAG);
        else
            Log.e(TAG, "Can not start fragment, context parameter is not an instance of any proper components!");
    }
}
