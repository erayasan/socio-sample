package com.erayasan.socio_sample.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.erayasan.socio_sample.R;
import com.erayasan.socio_sample.model.FilterProps;
import com.erayasan.socio_sample.viewmodel.MainViewModel;
import com.jaygoo.widget.RangeSeekBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by erayasan on 13/03/2018.
 */

public class FilterBottomSheetFragment extends BottomSheetDialogFragment {

    public static final String TAG = FilterBottomSheetFragment.class.getSimpleName();

    private Unbinder unbinder;
    private MainViewModel mainViewModel;

    @BindView(R.id.rsb_age_filter)
    RangeSeekBar ageSeekBar;

    public static FilterBottomSheetFragment newInstance() {

        Bundle args = new Bundle();

        FilterBottomSheetFragment fragment = new FilterBottomSheetFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_sheet_filter, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mainViewModel.getFilterProps() == null)
            ageSeekBar.setValue(ageSeekBar.getMin(), ageSeekBar.getMax());
        else
            ageSeekBar.setValue(mainViewModel.getFilterProps().getLowerAgeLimit(), mainViewModel.getFilterProps().getHigherAgeLimit());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_apply_filter)
    void onApplyFilterButtonClicked() {
        mainViewModel.actionFilterClicked(new FilterProps((int) ageSeekBar.getCurrentRange()[0], (int) ageSeekBar.getCurrentRange()[1]));
        dismiss();
    }
}
