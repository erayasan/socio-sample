package com.erayasan.socio_sample.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.erayasan.socio_sample.R;
import com.erayasan.socio_sample.adapter.UserProfilesRecyclerAdapter;
import com.erayasan.socio_sample.core.BaseActivity;
import com.erayasan.socio_sample.fragment.FilterBottomSheetFragment;
import com.erayasan.socio_sample.viewmodel.MainViewModel;
import com.erayasan.socio_sample.widget.SearchToolbar;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @BindView(R.id.rcy_user_profiles)
    RecyclerView userProfilesRecycler;
    @BindView(R.id.srcbar_main_search_toolbar)
    SearchToolbar searchToolbar;

    UserProfilesRecyclerAdapter adapter;

    MainViewModel mainViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainViewModel = ViewModelProviders.of(MainActivity.this).get(MainViewModel.class);

        initRecyclerView();
        initSearchToolbar();

        mainViewModel.getUserProfiles().observe(this, userProfiles -> {
            if (userProfiles != null) {
                adapter.setList(userProfiles);
                userProfilesRecycler.scrollToPosition(0);
            }
        });

        mainViewModel.requestUserProfilesFromNetwork();
    }

    private void initRecyclerView() {
        userProfilesRecycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new UserProfilesRecyclerAdapter();
        userProfilesRecycler.setAdapter(adapter);
    }

    private void initSearchToolbar() {
        searchToolbar.setOnSearchToolbarTextChangedListener(new SearchToolbar.OnSearchToolbarTextChangedListener() {
            @Override
            public void onQueryTextChange(String text) {
                mainViewModel.instantSearch(text);
            }

            @Override
            public void onQueryTextSubmit(String query) {

            }
        });
    }

    @OnClick(R.id.fab_filter)
    void onFilterButtonClicked() {
        FilterBottomSheetFragment.newInstance().show(getSupportFragmentManager(), FilterBottomSheetFragment.TAG);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }
}
